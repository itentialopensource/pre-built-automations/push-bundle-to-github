
## 0.0.13 [12-04-2023]

* Removes images

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!26

---

## 0.0.12 [09-25-2023]

* fix readme deprecation notice

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!23

---

## 0.0.11 [05-26-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!19

---

## 0.0.10 [07-08-2022]

* patch/DSUP-1361

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!18

---

## 0.0.9 [06-07-2022]

* Re certified for 2021.2

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!17

---

## 0.0.8 [05-17-2022]

* Patch/dsup 1307

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!16

---

## 0.0.6-2021.1.2 [03-23-2022]

* Patch/dsup 1324

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!12

---

## 0.0.6-2021.1.1 [02-14-2022]

* Patch/dsup 1294

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!11

---

## 0.0.6-2021.1.0 [08-06-2021]

* Minor issues with Push Bundle to Github Update

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!8

---

## 0.0.6 [08-06-2021]

* Minor issues with Push Bundle to Github Update

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!8

---

## 0.0.5 [07-28-2021]

* Update bundles/transformations/updatePackageGitHubJSON.json,...

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!7

---

## 0.0.4 [07-03-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!6

---

## 0.0.3 [04-15-2021]

* added app-artifact iap dependency to pre-built

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!5

---

## 0.0.2 [04-15-2021]

* Update package-lock.json, package.json files

See merge request itentialopensource/pre-built-automations/push-bundle-to-github!4

---

## 7.0.0 [04-15-2021]

* major/2021-04-08T11-01-44

See merge request itentialopensource/pre-built-automations/staging/push-bundle-to-github!3

---

## 6.0.0 [04-15-2021]

* major/2021-04-08T11-01-44

See merge request itentialopensource/pre-built-automations/staging/push-bundle-to-github!3

---

## 5.0.0 [04-15-2021]

* major/2021-04-08T11-01-44

See merge request itentialopensource/pre-built-automations/staging/push-bundle-to-github!3

---

## 4.0.0 [04-14-2021]

* major/2021-04-08T11-01-44

See merge request itentialopensource/pre-built-automations/staging/push-bundle-to-github!3

---

## 3.0.0 [04-09-2021]

* major/2021-04-08T11-01-44

See merge request itentialopensource/pre-built-automations/staging/push-bundle-to-github!3

---

## 2.0.0 [04-08-2021]

* major/2021-04-08T11-01-44

See merge request itentialopensource/pre-built-automations/staging/push-bundle-to-github!3

---

## 1.0.0 [04-08-2021]

* major/2021-04-08T09-30-02

See merge request itentialopensource/pre-built-automations/staging/push-bundle-to-github!2

---

## 0.0.2 [03-27-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n
